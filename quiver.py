from pathlib import Path
import numpy as np
import cv2 as cv
from PIL import Image
import matplotlib.pyplot as plt

#---------------------------------------
#Funcion para leer el archivo .flo----> readFlow()
#https://github.com/lmb-freiburg/flownet2/blob/master/scripts/run-flownet.py#L100-L115
#---------------------------------------
#Plotear el quiver a partir de un archivo .flo 
#https://stackoverflow.com/questions/61943240/quiver-plot-with-optical-flow
#---------------------------------------
def readFlow(name):
    if name.endswith('.pfm') or name.endswith('.PFM'):
        return readPFM(name)[0][:,:,0:2]

    f = open(name, 'rb')

    header = f.read(4)
    if header.decode("utf-8") != 'PIEH':
        raise Exception('Flow file header does not contain PIEH')

    width = np.fromfile(f, np.int32, 1).squeeze()
    height = np.fromfile(f, np.int32, 1).squeeze()

    flow = np.fromfile(f, np.float32, width * height * 2).reshape((height, width, 2))

    return flow.astype(np.float32)

out_p="/Users/sam/Documents/Proyecto_Graduacion/Pruebas/resultado0129/p.flo"
flow =readFlow(out_p)

step = 3
plt.quiver(np.arange(0, flow.shape[1], step), np.arange(flow.shape[0], -1, -step), flow[::step, ::step, 0], flow[::step, ::step, 1])
plt.show()



