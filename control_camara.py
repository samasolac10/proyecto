import bpy

#Posicion de la camara
tx = 0.0
ty = 0.0
tz = 80.0

#Angulos de la camara
rx = 0.0
ry = 0.0
rz = 0.0

pi = 3.14159265

scene = bpy.data.scenes["Scene"]

# Rotacion de la camara
scene.camera.rotation_mode = 'XYZ'
scene.camera.rotation_euler[0] = rx*(pi/180.0)
scene.camera.rotation_euler[1] = ry*(pi/180.0)
scene.camera.rotation_euler[2] = rz*(pi/180.0)

# Traslacion de la camara
scene.camera.location.x = tx
scene.camera.location.y = ty
scene.camera.location.z = tz
