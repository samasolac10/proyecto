
#Smoothness modifier options
bpy.context.space_data.context = 'MODIFIER'
bpy.ops.object.modifier_add(type='SUBSURF')
bpy.context.object.modifiers["Subdivision"].levels = 3
bpy.context.object.modifiers["Subdivision"].render_levels = 3

#Material modification
bpy.context.space_data.context = 'MATERIAL'
bpy.ops.material.new()
bpy.context.object.active_material.name = "material_agua"
bpy.data.materials["material_agua"].node_tree.nodes["Principled BSDF"].inputs[7].default_value = 0.083
bpy.data.materials["material_agua"].node_tree.nodes["Principled BSDF"].inputs[15].default_value = 1
bpy.data.materials["material_agua"].node_tree.nodes["Principled BSDF"].inputs[14].default_value = 1.3
bpy.context.object.active_material.use_screen_refraction = True


#Render options 
bpy.context.space_data.context = 'RENDER'
bpy.context.scene.eevee.use_ssr = True
bpy.context.scene.eevee.use_ssr_refraction = True

