import array
import Imath
import numpy as np
import cv2
import OpenEXR
import Imath
import array
import csv
import time
import datetime
import matplotlib.pyplot as plt


def exr2flow(exr,w,h,out_p):
  file = OpenEXR.InputFile(exr)    #used to read an EXR file
  # Compute the size
  dw = file.header()['dataWindow'] #Extrae info del archivo, max y min 

  sz = (dw.max.x - dw.min.x + 1, dw.max.y - dw.min.y + 1) #sz=(res_x,res_y)
  resx = dw.max.x+1
  resy = dw.max.y+1

  # Read the three color channels as 32-bit floats
  FLOAT   = Imath.PixelType(Imath.PixelType.FLOAT)
  (R,G,B) = [array.array('f', file.channel(Chan, FLOAT)).tolist() for Chan in ("R", "G", "B") ]# Convert to list
  
  img = np.zeros((h,w,2), np.float64)# img.shape->(res_X,res_y,canales=2)

  img[:,:,0] = -np.array(R).reshape(img.shape[0],-1)
  img[:,:,1] = -np.array(G).reshape(img.shape[0],-1)
  #img = np.flip(img, 0)
  #img = np.flip(img, 1)
  
  #img=img*0.05
  #Create and open the .flo file
  obj_out = open(out_p, 'wb')

  np.array([ 80, 73, 69, 72 ], np.uint8).tofile(obj_out)#Save the Header to the .flo file
  np.array([ resx, resy ], np.int32).tofile(obj_out)    #width as an integer height as an integer
  np.array(img, np.float32).tofile(obj_out)
  
  obj_out.close()
  return img

exr    = "/Users/sam/Desktop/Image0213.exr"
salida = '/Users/sam/Desktop/s.flo'
img=exr2flow(exr, 640,380,salida)
